const http = require("http");

const port = 8000;

let callback = (returnFn) => (response) => {
  let str = "";
  response.on("data", (chunk) => {
    str += chunk;
  });
  response.on("end", () => {
    returnFn(str);
    console.log(JSON.parse(str));
  });
};

http
  .createServer(function (req, res) {
    const sendHtml = (data) => {
      res.writeHead(200, { "Content-Type": "application-json" });
      res.write(data);
      res.end();
    };

    const url = req.url;

    function extractCountry() {
      let str = url.slice(12);
      return str;
    }

    //Routes
    if (req.method === "GET") {
      if (url === "/home") {
        res.write("welcome to home");
        res.end();
      } else if (url === "/about") {
        res.write("welcome to about");
        res.end();
      } else {
        res.write("hello world!");

        res.end();
      }
    } else if (req.method === "POST") {
      let country = extractCountry();
      if (url === `/university/${country}`) {
        http
          .request(
            `http://universities.hipolabs.com/search?country=${country}`,
            callback(sendHtml)
          )
          .end();
      } else {
        let data = "hello";
        req.on("data", (chunk) => {
          data += chunk;
        });
        req.on("end", () => {
          res.end(data);
        });
      }
    }
  })
  .listen(port, "127.0.0.1", () => {
    console.log(`server runnning on port ${port}...`);
  });
